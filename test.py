import os, sys, unittest, argparse

from testing.client.pipeline.graph import CaseGraph
from testing.client.pipeline.job import CaseJob
from testing.client.pipeline.run import CaseRun

if __name__ == '__main__':

    # Create test loader
    loader = unittest.TestLoader()

    # Create test suite
    suite = unittest.TestSuite((
        loader.loadTestsFromTestCase(CaseGraph),
        loader.loadTestsFromTestCase(CaseJob),
        loader.loadTestsFromTestCase(CaseRun),
    ))

    # Create test runner
    runner = unittest.TextTestRunner(verbosity=2)

    # Stop as soon as we encounter a failed unit test
    runner.failfast = True

    # Run the test suite
    test_results = runner.run(suite)

    # Return the proper exit code (important for continuous deployment)
    sys.exit(not test_results.wasSuccessful())