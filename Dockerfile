FROM python:3.5

MAINTAINER Thomas Kroes "t.kroes@lumc.nl"

ADD ./requirements.txt /requirements.txt

RUN apt-get update
RUN pip install -r /requirements.txt

RUN mkdir /pim
ADD ./ /pim

CMD gunicorn -w 4 --bind 0.0.0.0:5000 "server.app:create_app_from_config('production')"