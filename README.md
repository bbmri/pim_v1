# PIM

**PIM** is a generic web framework for online inspection and monitoring of lengthy processing pipelines. The aim of this 
project is to provide end users (researchers) intuitive and easily accessible insight into the structure and progress of 
 processing pipelines. Our system provides the end user with a comprehensible and user friendly visual representation of 
 a processing pipeline, including detailed progress information. We exploit the concept of ‘details on demand’ to 
 provide a rich experience where high level overview is combined with fine grained information (e.g. succeeded or failed 
 jobs). The researcher does not need a technical background to monitor progress and to inspect (intermediate) results. 
 PIM only requires a modern browser.

Although we tested PIM in the context of image processing (as part of the [BBMRI-NL2.0 
project - WP3](https://www.bbmri.nl/)), it is pipeline 
execution engine agnostic. Other fields that use lengthy processing pipelines such as genetics and metabolomics can 
benefit from PIM due to it’s loose coupling. 

![smiley](./pim.png)
*Example of an image processing pipeline in the PIM web interface*

## Running PIM
The most convenient way to run PIM locally or remotely is to run the fully pre-configured environment using [Docker (17.06.1-ce)](https://www.docker.com/) and [Docker 
Compose (1.13.0)](https://docs.docker.com/compose/). 

First, clone [this](https://gitlab.com/bbmri/pim) repository and navigate to the PIM directory:

    $ git clone https://gitlab.com/bbmri/pim
    $ cd pim

In the PIM directory, run the [Docker Compose file](/docker-compose.yml), this will start all the necessary interlinked 
services ([Flask](http://flask.pocoo.org/), [MongoDB](https://www.mongodb.com/) and 
[NGINX](https://www.nginx.com/resources/wiki/)):

    $ sudo docker-compose up
    
Open a browser (preferably [Google Chrome](https://www.google.nl/chrome/browser/desktop/index.html)), and navigate to 
[localhost](http://localhost). This will bring you to the pipeline run management page. There are currently no runs in
the database. However, you can populate the PIM database with example data by using the shipped pipeline run 
[simulator](/simulator). For this you need to run [simulate.py](/simulator/simulate.py): First add the PIM clone dir to
the PYTHONATH:

    $ export PYTHONPATH=$PYTHONPATH:pim_dir

Then run [simulate.py](/simulator/simulate.py) with the PIM_SERVER_URL set to localhost:

    $ PIM_SERVER_URL=http://localhost python3 pim_dir/simulator/simulate.py
   
On the management page you should now see three runs: *grouping_test*, *large_test* and *minimal_test*. Click view to 
explore a pipeline run.

### API documentation
If you want to connect your own pipeline execution engine to PIM, you should take a look at the swagger documentation,
available at [http://localhost/api/documentation](http://localhost/api/documentation).

## Running the tests
Basic testing has been implemented that tests the PIM restful API. Follow the steps below to perform the tests:

    $ cd /pim_dir
    $ PIM_CONFIG=development python3 ./test.py

