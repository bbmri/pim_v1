import os
import json
import unittest

from server.app import create_app_from_config

class ClientTestCaseBase(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def clear_mongo(self):
        self.test_client.application.mongodb.reset()

    def setUp(self):

        # Get PIM config type from environment
        pim_config = os.environ.get('PIM_CONFIG', 'testing')

        # Create testing Flask application
        self.app            = create_app_from_config(pim_config)
        self.test_client    = self.app.test_client()

        # Remove previous test data
        self.clear_mongo()

    def tearDown(self):
        self.clear_mongo()

    def http_put_run(self, run_json, expected_status_code=200):
        """Add run to PIM with HTTP PUT request

        :param run_json: Run in JSON format
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Setup HTTP request headers
        headers = [('Content-Type', 'application/json')]
        headers.append(('Content-Length', len(run_json)))

        # And add to PIM in an HTTP PUT request
        http_request = self.test_client.put('/api/runs/', headers=headers, data=json.dumps(run_json))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=json.loads(http_request.get_data(as_text=True)))

    def http_get_run(self, run_id, expected_status_code=200):
        """Get run from PIM with HTTP GET request

        :param run_id: Run ID
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Get the run from the PIM server in an HTTP GET request
        http_request = self.test_client.get('/api/runs/{}'.format(run_id))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=json.loads(http_request.get_data(as_text=True)))

    def http_delete_run(self, run_id, expected_status_code=200):
        """Remove run from PIM with HTTP DELETE request

        :param run_id: Run ID
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Remove the run from the PIM server in an HTTP DELETE request
        http_request = self.test_client.delete('/api/runs/{}'.format(run_id))

        # Test HTTP status code
        if expected_status_code >= 0:
            self.assertEqual(http_request.status_code, expected_status_code, msg=json.loads(http_request.get_data(as_text=True)))

    def http_put_job(self, run_id, job_json, expected_status_code=200):
        """Add job to PIM with HTTP PUT request

        :param run_id: Run ID
        :param job_json: Job in JSON format
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Setup HTTP request headers
        headers = [('Content-Type', 'application/json')]
        headers.append(('Content-Length', len(job_json)))

        # Add to PIM server in an HTTP PUT request
        http_request = self.test_client.put('/api/runs/{}/jobs/{}'.format(run_id, job_json['id']), headers=headers, data=json.dumps(job_json))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=json.loads(http_request.get_data(as_text=True)))

    def http_get_job(self, run_id, job_id, expected_status_code=200):
        """Retrieve job from PIM with HTTP GET request

        :param run_id: Run ID
        :param job_id: Job ID
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Get the job from the PIM server in an HTTP GET request
        http_request = self.test_client.get('/api/runs/{}/jobs/{}'.format(run_id, job_id))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=json.loads(http_request.get_data(as_text=True)))

    def http_delete_job(self, run_id, job_id, expected_status_code=200):
        """Remove job from PIM with HTTP DELETE request

        :param run_id: Run ID
        :param job_id: Job ID
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Remove the job from PIM in an HTTP delete request
        http_request = self.test_client.delete('/api/runs/{}/jobs/{}'.format(run_id, job_id))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=json.loads(http_request.get_data(as_text=True)))

    def http_get_graph(self, run_id, expected_status_code=200):
        """Get kieler graph JSON from PIM with HTTP GET request

        :param run_id: Run ID
        :param client_id: Client ID
        :param expected_status_code: The HTTP error code that is expected from the operation
        """

        # Get the run from the PIM server in an HTTP GET request
        http_request = self.test_client.get('/api/runs/{}/graph'.format(run_id))

        # Get JSON response
        repsonse_json = json.loads(http_request.get_data(as_text=True))

        # Test HTTP status code
        self.assertEqual(http_request.status_code, expected_status_code, msg=repsonse_json)

        return repsonse_json