from simulator.job import Job
from simulator.examples.minimal import create_run
from testing.client.base import ClientTestCaseBase


class CaseJob(ClientTestCaseBase):
    def setUp(self):
        super(CaseJob, self).setUp()

        # Case name from module name
        case_name = __name__.split('.')[-1]

        # Create run
        self.run = create_run()

        # Add the run in an HTTP put request
        self.http_put_run(self.run.to_json())

    def tearDown(self):
        super(CaseJob, self).tearDown()

        # Remove the run in an HTTP delete request
        self.http_delete_run(self.run.id, -1)

    def test_basic(self):
        """Basic job test"""

        # Create job
        job = Job(run_id=self.run.id, node_id=self.run.network.nodes['source'].id, sample_id='0')

        # Add the job in an HTTP put request
        self.http_put_job(self.run.id, job.to_json())

        # And verify that it exists with an HTTP get request
        self.http_get_job(self.run.id, job.id)

        # Delete the job with an HTTP delete request
        self.http_delete_job(self.run.id, job.id)

        # And verify that the job is removed with an HTTP get request
        self.http_get_job(self.run.id, job.id, 404)

    def test_no_run(self):
        """Add job no non-existing run test"""

        # Remove the run in an HTTP delete request
        self.http_delete_run(self.run.id)

        # Create job
        job = Job(run_id='', node_id='', sample_id='0')

        # Add the job in an HTTP put request
        self.http_put_job('undefined', job.to_json(), 404)