import time

from simulator.job import Job

from testing.client.base import ClientTestCaseBase
from simulator.examples.minimal import create_run


class CaseGraph(ClientTestCaseBase):
    def setUp(self):
        super(CaseGraph, self).setUp()

        # Case name from module name
        case_name = __name__.split('.')[-1]

        # Create run
        self.run = create_run()

        # Add the run in an HTTP put request
        self.http_put_run(self.run.to_json())

    def tearDown(self):
        super(CaseGraph, self).tearDown()

        # Remove the run in an HTTP delete request
        self.http_delete_run(self.run.id, -1)

    def test_basic(self):
        """Basic graph test"""

        # Get graph using HTTP get request
        self.http_get_graph(run_id=self.run.id)

    def test_caching_a(self):
        """Caching functionality test A"""

        # Get two graphs in succession using two HTTP get requests
        graph_a = self.http_get_graph(run_id=self.run.id)
        graph_b = self.http_get_graph(run_id=self.run.id)

        # Due to caching both graphs should have the same time stamp
        self.assertEqual(graph_a['last_modified'], graph_b['last_modified'])

    def test_caching_b(self):
        """Caching functionality test B"""

        # Get a fresh graph copy
        graph_a = self.http_get_graph(run_id=self.run.id)

        # Create job
        job = Job(run_id=self.run.id, node_id=self.run.network.nodes['source'].id, sample_id='0')

        # Add the job in an HTTP put request
        self.http_put_job(self.run.id, job.to_json())

        # Get a (should be cached) graph copy
        graph_b = self.http_get_graph(run_id=self.run.id)

        # Test if the caching succeeded
        self.assertEqual(graph_a['last_modified'], graph_b['last_modified'])

        # Wait beyond the caching interval
        time.sleep(2.0 * (self.test_client.application.graph_caching_interval / 1000.0))

        # Get a (should NOT be cached) graph copy
        graph_c = self.http_get_graph(run_id=self.run.id)

        # There should be caching at play here
        self.assertGreater(graph_c['last_modified'], graph_b['last_modified'])

    def test_caching_c(self):
        """Caching functionality test C"""

        # Get two graphs in succession using two HTTP get requests
        graph_a = self.http_get_graph(run_id=self.run.id)

        # # Due to caching both graphs should have the same time stamp
        # self.assertEqual(graph_a['last_modified'], graph_b['last_modified'])
        #
        # # Wait beyond the caching interval
        # time.sleep(2.0 * (self.test_client.application.graph_caching_interval / 1000.0))
        #
        # # Get a new graph using an HTTP get request
        # graph_c = self.http_get_graph(run_id=self.run.id)
        #
        # # The request was beyond the caching interval, but no jobs have been added, so no caching should take place
        # # self.assertEqual(graph_c['last_modified'], graph_b['last_modified'])
        #
        # # Create job
        # job = Job(run_id=self.run.id, node_id=self.run.network.nodes['source'].id, sample_id='0')
        #
        # # Add the job in an HTTP put request
        # self.http_put_job(self.run.id, job.to_json())
        #
        # # Get a new graph using an HTTP get request
        # graph_d = self.http_get_graph(run_id=self.run.id)
        #
        # # The graph cache should now be invalidated and a newer graph version should be available
        # self.assertGreater(graph_d['last_modified'], graph_c['last_modified'])