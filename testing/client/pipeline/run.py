import json
import os

from simulator.group import Group
from simulator.node import Node
from simulator.examples.minimal import create_run

from testing.client.base import ClientTestCaseBase


class CaseRun(ClientTestCaseBase):
    def setUp(self):
        super(CaseRun, self).setUp()

        # Create test run
        self.run = create_run()

    def test_basic(self):
        """Basic run test"""

        # Add the run in an HTTP put request
        self.http_put_run(self.run.to_json())

        # And verify that it exists
        self.http_get_run(self.run.id)

        # Delete it in an HTTP delete request
        self.http_delete_run(self.run.id)

        # The run should not be available, so a HTTP get request should yield a 404 status code
        self.http_get_run(self.run.id, 404)

    def test_invalid_group_id(self):
        """Invalid group test"""

        # Deliberately set the source node group id to a non-existing group as it should cause failure
        self.run.network.nodes['source'].group_id = ''

        # Add the run in an HTTP put request, this test should fail with a bad request 400 HTTP error code
        self.http_put_run(self.run.to_json(), expected_status_code=400)

    def test_invalid_link_node(self):
        """Invalid link node test"""

        # Deliberately compromise the integrity of a link by invalidating the from node
        self.run.network.links[0].from_node = None

        # Add the run in an HTTP put request, this test should fail with a bad request 400 HTTP error code
        self.http_put_run(self.run.to_json(), expected_status_code=400)

    def test_invalid_link_port(self):
        """Invalid link port test"""

        # Deliberately compromise the integrity of a link by invalidating the from port
        self.run.network.links[0].from_port = None

        # Add the run in an HTTP put request, this test should fail with a bad request 400 HTTP error code
        self.http_put_run(self.run.to_json(), expected_status_code=400)

    def test_invalid_parent_group(self):
        """Invalid parent group test"""

        # Push a network containing with a portless node to PIM, this test should fail with a bad request 400 HTTP error code
        with open('./testing/client/pipeline/fail/invalid_parent_group.json') as json_data:
            self.http_put_run(json.load(json_data), expected_status_code=400)

    def test_portless_node(self):
        """Portless node test"""

        # Push a network containing with a portless node to PIM, this test should fail with a bad request 400 HTTP error code
        with open('./testing/client/pipeline/fail/portless_node.json') as json_data:
            self.http_put_run(json.load(json_data), expected_status_code=400)

    def test_empty_group(self):
        """Empty group test"""

        # Push a network containing with an empty group to PIM, this test should fail with a bad request 400 HTTP error code
        with open('./testing/client/pipeline/fail/empty_group.json') as json_data:
            self.http_put_run(json.load(json_data), expected_status_code=400)

    def test_duplicate_group_ids(self):
        """Test duplicate group ID's"""

        # Push a network containing two identical groups to PIM, this test should fail with a bad request 400 HTTP error code
        with open('./testing/client/pipeline/fail/duplicate_group_ids.json') as json_data:
            self.http_put_run(json.load(json_data), expected_status_code=400)

    def test_duplicate_node_ids(self):
        """Test duplicate node ID's"""

        # Push a network containing two identical nodes to PIM, this test should fail with a bad request 400 HTTP error code
        with open('./testing/client/pipeline/fail/duplicate_node_ids.json') as json_data:
            self.http_put_run(json.load(json_data), expected_status_code=400)
