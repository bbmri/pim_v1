d3.create_group = function() {

    function my(selection) {
        selection.each(function(d, i) {

            // Add background element
            d3.select(this).append("rect")
                .attr("class", "background level_" + d.level)
                .attr("rx", 4)
                .attr("ry", 4)
                .attr("width", d.width)
                .attr("height", d.height);

            // Add header text element
            var header_text = d3.select(this).append("text")
                .attr("class", "header")
                .attr("x", d.width / 2)
                .attr("y", 15)
                .attr("text-anchor", "middle");

            // Add id tspan element to the header text
            header_text.append("tspan")
                .attr("class", "id")
                .text("");

            // Add progress tspan element to the header text
            header_text.append("tspan")
                .attr("class", "progress")
                .text("");

            // Add legend text
            d3.select(this).append("text")
                .attr("class", "legend")
                .attr("x", d.width / 2)
                .attr("y", 30)
                .attr("text-anchor", "middle");

            // Add progress element
            var progress = d3.progress()
                .width(function(d) { return d.width - 10; })
                .height(7);

            d3.select(this).append("g")
                .attr("transform", function(d) { return "translate(" +  10 + ", " + 37 + ")"; })
                .call(progress);

            d3.select(this).append("line")
                .attr("x1", 0)
                .attr("x2", function(d) { return d.width; })
                .attr("y1", 55)
                .attr("y2", 55)
                .attr("stroke-width", 0.5)
                .attr("stroke", "rgb(100, 100, 100)")
                .attr("stroke-dasharray", "2, 2");
        });
    }

    return my;
};

d3.update_group = function() {

    function my(selection) {
        selection.each(function(d, i) {

            d3.select(this).select(".id")
                .text(function(d) { return d.id; });

            d3.select(this).select(".progress")
                .text(function(d) { return " (" + (100 * d.status.progress).toFixed(1) + "%)"; });

            var update_progress = d3.update_progress()
                .width(function(d) { return d.width - 20; })
                .height(7);

            d3.select(this).call(update_progress);

            // Update the legend
            var legend_data = d3.select(this).select(".legend").selectAll("tspan")
                .data(d.status.jobs);

            legend_data.enter()
                .append("tspan");

            legend_data
                  .text(function(d) { return " " + pad(d.value, 3) + " " + d.name; });

            legend_data.exit().remove();
        });
    }

    return my;
};