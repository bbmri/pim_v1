d3.progress = function() {

    var width       = 25;
    var height      = 25;

    function my(selection) {
        selection.each(function(d, i) {




        });
    }

    // Getter/setter for the width property
    my.width = function(value) {
        if (!arguments.length)
            return width;

        width = value;
        return my;
    };

    // Getter/setter for the height property
    my.height = function(value) {
        if (!arguments.length)
            return height;

        height = value;
        return my;
    };

    return my;
};

d3.update_progress = function() {

    var width       = 25;
    var height      = 25;

    function my(selection) {
        selection.each(function(d, i) {

            // Evaluate properties (possibly with anonymous function)
            var _width          = (typeof(width) === "function" ? width(d) : width);
            var _height         = (typeof(height) === "function" ? height(d) : height);

            var bar_group = d3.select(this).select("g");

            var progress_data = [];//new Array(4);

            // Sort ids to ensure that the progress sections are drawn in the correct order
            var sort_ids = { idle: 0, running: 1, success: 2, failed: 3 };

            if (d.status.no_jobs > 0) {

                /*
                var sorted_jobs = new Array(4);

                // Sort
                d.status.jobs.forEach(function (element) {
                    sorted_jobs[sort_ids[element["name"]]] = element;
                });
                */

                // Incremental offset
                var offset = 0;

                // Populate progress data
                d.status.jobs.forEach(function (job) {
                    var section = {}

                    // Values from kgraph
                    section["name"]         = job["name"];
                    section["offset"]       = 0;
                    section["value"]        = job["value"];
                    section["norm_value"]   = job["value"];
                    section["percentage"]   = 100 * (section["value"] / d.status.no_jobs);
                    section["text"]         = "";

                    // Add dictionary to progress data list
                    progress_data[sort_ids[section["name"]]] = section;
                    // progress_data.push(section);
                });

                // Normalize the offsets and values
                progress_data.forEach(function(element) {
                    element["offset"] = offset;

                    offset += element["value"]
                });

                // Offset now contains the sum of all progress items
                var progress_sum = offset;

                var problem = false;

                // Normalize the offsets and values
                progress_data.forEach(function(element) {
                    element["norm_value"]   /= progress_sum;
                    element["offset"]       /= progress_sum;// * 2;

                    // if (element["name"] == "running")
                    //     element["norm_value"] = 0;
                });
            }
            /*
            else
            {
                // No jobs, so generate single bar with informative text
                progress_data = [];

                var section = {};

                section["name"]         = "scheduled";
                section["text"]         = "There are no jobs associated with this node/group";
                section["value"]        = 0;
                section["norm_value"]   = 1;
                section["offset"]       = 0;
                section["percentage"]   = 0;

                progress_data.push(section);
            }
*/
            // Generate bar tooltip
            var tooltip = function(d) {

                // Handle special case when there are no jobs associated with the node or group
                if (d.value == 0)
                    return d.text;

                // Provide additional status information for a specific job type
                if (d.value > 1)
                    return d.value + " job(s) " + d.name + " " + d.percentage.toFixed(1) + "%";
                else
                    return d.value + " job " + d.name + " " + d.percentage.toFixed(1) + "%";
            }

            var rect = bar_group.selectAll('rect')
		        .data(progress_data);

            var rect_enter = rect
                .enter()
                .append('rect')
                .append("title")
                    .text(tooltip)
                .transition()
                    .duration(100)
                    .ease("linear")
                    .attr("class", function(d) { return d.name; })
                    .attr("x", function(d) { return d.offset * _width; })
                    .attr("width", function(d) { return d.norm_value * _width; })
                    .attr("height", function(d) { return _height; });

            rect.transition()
                .duration(100) // Also NEW
                .ease("linear")
                .attr("class", function(d) { return d.name; })
                .attr("x", function(d) { return d.offset * _width; })
                .attr("width", function(d) { return d.norm_value * _width; })
                .attr("height", function(d) { return _height; });

            rect.exit().remove();

            // Add outline element
            bar_group.append("rect")
                .attr("width", _width)
                .attr("height", _height)
                .attr("fill", "none")
                .attr("stroke-width", 0.5)
                .attr("stroke", "rgb(20, 20, 20)");

        });
    }

    // Getter/setter for the width property
    my.width = function(value) {
        if (!arguments.length)
            return width;

        width = value;
        return my;
    };

    // Getter/setter for the height property
    my.height = function(value) {
        if (!arguments.length)
            return height;

        height = value;
        return my;
    };

    return my;
};