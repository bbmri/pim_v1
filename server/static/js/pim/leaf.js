d3.create_leaf = function() {

    function my(selection) {
        selection.each(function(d, i) {

            // Add background element
            var background = d3.select(this).append("rect")
                .attr("class", "background " + d.type)
                .attr("rx", 5)
                .attr("ry", 5)
                .attr("width", d.width)
                .attr("height", d.height);

            var symbol_map  = { constant: "table2", node: "cogs", source: "file-picture", sink: "download" }
            var symbol      = "http://127.0.0.1:5000/static/symbol-defs.svg#icon-" + symbol_map[d.type];
            var icon_size   = { width: 50, height: 50 };

            /*
            d3.select(this).append("svg")
                //.attr("viewBox", "0 0 64 64")
                .attr("x", (d.width / 2) - (icon_size.width / 2))
                .attr("y", (d.height / 2) - (icon_size.height / 2))
                .append("use")
                    .attr("xlink:href", symbol).attr("width", icon_size.width).attr("height", icon_size.height)
                .style("opacity", 0.05);
            */

            // Add header text element
            var header_text = d3.select(this).append("text")
                .attr("class", "header")
                .attr("x", d.width / 2)
                .attr("y", 12)
                .attr("text-anchor", "middle");

            // Add id tspan element to the header text
            header_text.append("tspan")
                .attr("class", "id icon-home")
                .text("");

            // Add progress tspan element to the header text
            header_text.append("tspan")
                .attr("class", "progress")
                .text("");

            d3.select(this).append("line")
                .attr("x1", 0)
                .attr("x2", function(d) { return d.width; })
                .attr("y1", 24)
                .attr("y2", 24)
                .attr("stroke-width", 0.5)
                .attr("stroke", "rgb(100, 100, 100)")
                .attr("stroke-dasharray", "2, 2");

            // Add progress element
            var progress = d3.progress()
                .width(function(d) { return d.width - 10; })
                .height(6);

            d3.select(this).append("g")
                .attr("transform", function(d) { return "translate(" +  10 + ", " + 45 + ")"; })
                .call(progress);

            // Add legend group
            var legend_text = d3.select(this).append("text")
                .attr("class", "legend")
                .attr("x", d.width / 2)
                .attr("y", 37)
                .attr("text-anchor", "middle");
        });
    }

    return my;
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = " " + s;
    return s;
}

d3.update_leaf = function() {

    function my(selection) {
        selection.each(function(d, i) {

            d3.select(this).select(".header .id")
                .text(function(d) { return d.id; });

            d3.select(this).select(".header .progress")
                .text(function(d) { return " (" + (100 * d.status.progress).toFixed(1) + "%)"; });

            var update_progress = d3.update_progress()
                .width(function(d) { return d.width - 20; })
                .height(6);

            d3.select(this).call(update_progress);

            // Update the legend
            var legend_data = d3.select(this).select(".legend").selectAll("tspan")
                .data(d.status.jobs);

            legend_data.enter()
                .append("tspan");

            legend_data
                  .text(function(d) { return " " + pad(d.value, 3) + " " + d.name; });

            legend_data.exit().remove();


           // d3.update_progress(d3.select(this).select("progress"));
        });
    }

    return my;
}