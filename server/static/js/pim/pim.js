/**
 * Created by thomas on 17-1-17.
 */


/*jshint strict:true */

// Obtain the run id by querying the run_id attribute of the html element (this mechanism needs to be improved)
var run_id = document.getElementById("pim").getAttribute("run_id");

function viewport() {
    var win = window;
    var inner = "inner";

    if (!("innerWidth" in window)) {
        inner = "client";
        win = document.documentElement || document.body;
    }

    return { width: win[inner + "Width"], height: win[inner + "Height"] }
}

var width   = viewport().width;
var height  = viewport().height;
var zoom    = d3.behavior.zoom()
    .on("zoom", redraw);

var idfun = function(d) { return d.id; };

var svg = d3.select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height)
    .call(zoom)
    .append("g");

var defs = svg.append("defs");

var root = svg.append("g");

var layout_valid = false;

// Node id mapped to position and size
var node_layout = {};

// Linear array of graph nodes with layout information
var nodes = [];

// Convert node tree to linear array of nodes
function flatten(node) {

    // Add node postion as computed by the layout algorithm
    node.x      = node_layout[node.id].x;
    node.y      = node_layout[node.id].y;
    node.width  = node_layout[node.id].width;
    node.height = node_layout[node.id].height;

    nodes.push(node);

    if (!node.children)
        return;

    node.children.forEach(function(child) {
        flatten(child);
    });
}

// Kieler layout algorithm
var layouter = klay.d3kgraph()
    .size([width, height])
    .transformGroup(root)
    .options({
        layoutHierarchy: true,
        intCoordinates: true,
        direction: "RIGHT",
        edgeRouting: "ORTHOGONAL", // SPLINES ORTHOGONAL
        nodeLayering: "NETWORK_SIMPLEX",
        nodePlace: "LINEAR_SEGMENTS", // BRANDES_KOEPF, LINEAR_SEGMENTS, INTERACTIVE, SIMPLE
        // mergeHierarchyPorts: true,
        crossMin: "LAYER_SWEEP",
        algorithm: "de.cau.cs.kieler.klay.layered"
    });

layouter.on("finish", function(d) {

    console.log("Layout finished, caching node positions");

    var layout_nodes = layouter.nodes();

    // Cache node positions
    for (var i = 0; i < layout_nodes.length; i++) {

        var layout_node = layout_nodes[i];

        // Add node id and position
        node_layout[layout_node.id] = { x: layout_node.x, y: layout_node.y, width: layout_node.width, height: layout_node.height };
    }

    // Clear node array
    nodes = [];

    // Flatten the node tree
    flatten(d.graph);

    var nodeData = root.selectAll(".node")
        .data(nodes, idfun);

    var node = nodeData.enter()
        .append("g")
        .call(d3.create_node());
});

function update_graph(graph) {

    if (layout_valid === false)
    {
        console.log("Performing layout");

        // Execute the graph layout
        layouter.kgraph(graph);

        // Perform layout only once
        layout_valid = true;
    }

    // Clear node array
    nodes = [];

    // Flatten the node tree
    flatten(graph);

    root.selectAll(".node")
        .data(nodes, idfun)
        .call(d3.update_node())

    var links = layouter.links(nodes);

    var link_data = root.selectAll(".link")
        .data(links, function(d) { return d.id; });

    var link_colors = {};

    // Enumerate link types
    links.forEach(function (link) {
        link_colors[link.type] = "";
    });

    var no_steps    = Object.keys(link_colors).length;
    var step        = 360 / no_steps;
    var hue         = 0
    var lightness   = 50;

    for (var link_type in link_colors)
    {
        var color = "hsl(" + hue + ", 45%, " + lightness + "%)";

        if (no_steps === 1) {
            color = "hsl(0, 0%, " + lightness + "%)";
        }

        link_colors[link_type] = color;

        // Increment the hue by the step size
        hue += step;
    }

    // add arrows
    var link = link_data.enter()
        .append("path")
        .attr("class", function(d) { return "link"; } )
        .attr("d", function(d) {
            var path = "";

            path += "M" + d.sourcePoint.x + " " + d.sourcePoint.y + " ";

            (d.bendPoints || []).forEach(function(bp, i) {
                path += "L" + bp.x + " " + bp.y + " ";
            });

            path += "L" + d.targetPoint.x + " " + d.targetPoint.y + " ";

            return path;
        })
        .attr("fill", "none")
        .attr("stroke-width", 1)
        .attr("stroke", function(d) { return link_colors[d.type]; })
        .style("opacity", 0);

    link.transition()
        .duration(500)
        .style("opacity", 1);

    // Apply edge routes
    /*
    link.transition().attr("d", function(d) {
        var path = "";

        path += "M" + d.sourcePoint.x + " " + d.sourcePoint.y + " ";

        (d.bendPoints || []).forEach(function(bp, i) {
            path += "L" + bp.x + " " + bp.y + " ";
        });

        path += "L" + d.targetPoint.x + " " + d.targetPoint.y + " ";

        return path;
    });
    */
}

// Graph time stamp
var last_modified = -1;

// Periodically update the graph visualization
setInterval(function()
{
    draw_graph();

}, 500);

function draw_graph() {
    var http_request = new XMLHttpRequest();

    http_request.onreadystatechange = function() {

        if (this.readyState === 4 && this.status === 200) {

            var response_json = JSON.parse(this.responseText);

            // And update the graph
            if (response_json['last_modified'] > last_modified) {
                // console.log('Updating graph');

                // Update the graph
                update_graph(response_json['graph']);

                // Update time stamp
                last_modified = response_json['last_modified'];
            }
            else {
               // console.log('Not updating graph');
            }
        }
    };

    http_request.open("GET", "../api/runs/" + run_id + "/graph", true);
    http_request.send();
}

function redraw() {
    svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
}