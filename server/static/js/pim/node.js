d3.create_node = function() {

    function my(selection) {
        selection.each(function(d, i) {

            if (d.children)
                d3.select(this).attr("class", "node group").call(d3.create_group());
            else
                d3.select(this).attr("class", "node leaf").call(d3.create_leaf());

            // Move to the right location
            d3.select(this).attr("transform", function(d) { return "translate(" +  d.x + ", " + d.y + ")"; });
        });
    }

    return my;
}

d3.update_node = function() {

    function my(selection) {
        selection.each(function(d, i) {

            if (d.children)
                d3.select(this).call(d3.update_group());
            else
                d3.select(this).call(d3.update_leaf());

        });
    }

    return my;
}



















