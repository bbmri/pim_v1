d3.separator = function() {
    var label   = "";
    var y       = 0;
    var width   = 25;

    function my(selection) {
        selection.each(function(d, i) {

            // Evaluate properties (possibly with anonymous function)
            var _label  = (typeof(label) === "function" ? label(d) : label);
            var _y      = (typeof(y) === "function" ? y(d) : y);
            var _width  = (typeof(width) === "function" ? width(d) : width);

            // Create svg group element
            var element = d3.select(this).append("g");

            // Add separator element
            var separator_element = element.append("line")
                .text(_label)
                .attr("x1", 0)
                .attr("x2", _width)
                .attr("y1", _y)
                .attr("y2", _y)
                .attr("stroke-width", 0.5)
                .attr("stroke", "rgb(100, 100, 100)")
                .attr("stroke-dasharray", "3, 3");

            // Move to the right location
            element.attr("transform", function(d) { return "translate(0", + _y + ")"; });
        });
    }

    // Getter/setter for the y-position property
    my.y = function(value) {
        if (!arguments.length)
            return y;

        y = value;
        return my;
    };

    // Getter/setter for the width property
    my.width = function(value) {
        if (!arguments.length)
            return width;

        width = value;
        return my;
    };

    return my;
}