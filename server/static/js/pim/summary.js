d3.summary = function() {
    var label   = "";
    var x       = 0;
    var y       = 0;
    var width   = 25;
    var height  = 25;

    function my(selection) {
        selection.each(function(d, i) {

            // Evaluate properties (possibly with anonymous function)
            var _label  = (typeof(label) === "function" ? label(d) : label);
            var _x      = (typeof(x) === "function" ? x(d) : x);
            var _y      = (typeof(y) === "function" ? y(d) : y);
            var _width  = (typeof(width) === "function" ? width(d) : width);
            var _height = (typeof(height) === "function" ? height(d) : height);

            // Get current element
            var element = d3.select(this).append("g")
                .attr("transform", function(d) { return "translate(" +  _x + ", " + _y + ")"; });

            // Add text element
            var text_element = element.append("text")
                .text(_label)
                .attr("x", _width / 2)
                .attr("y", _height / 2)
                .attr("alignment-baseline", "middle")
                .attr("text-anchor", "middle")
                .attr("font-size", "9px")
                .attr("font-weight", "normal")
                .attr("font-family", "Arial");
        });
    }

    // getter / setter for the label property
    my.label = function(value) {
        if (!arguments.length)
            return label;

        label = value;
        return my;
    };

    // Getter/setter for the x-position property
    my.x = function(value) {
        if (!arguments.length)
            return x;

        x = value;
        return my;
    };

    // Getter/setter for the y-position property
    my.y = function(value) {
        if (!arguments.length)
            return y;

        y = value;
        return my;
    };

    // Getter/setter for the width property
    my.width = function(value) {
        if (!arguments.length)
            return width;

        width = value;
        return my;
    };

    // Getter/setter for the height property
    my.height = function(value) {
        if (!arguments.length)
            return height;

        height = value;
        return my;
    };

    return my;
}