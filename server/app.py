import os
import logging
import argparse
import configparser

from flask import Flask, Blueprint
from pymongo import MongoClient

from server.api.restplus import api
from server.views.home import home_bp
from server.api.mongodb import MongoDB

from server.api.resources.run import ns as run_ns
from server.api.resources.runs import ns as runs_ns
from server.api.resources.job import ns as job_ns
from server.api.resources.jobs import ns as jobs_ns
from server.api.resources.graph import ns as graph_ns


import logging

werkzeug_logger = logging.getLogger('werkzeug')


def create_app(**kwargs):
    """Creates a custom PIM-Flask application

    :param kwargs: PIM-Flask configuration
    :return: Flask application
    """

    # Defaults from kwargs
    flask_debug         = kwargs.get('flask_debug', False)
    flask_profile       = kwargs.get('flask_profile', False)
    mongo_host          = kwargs.get('mongo_host', 'localhost')
    mongo_port          = kwargs.get('mongo_port', 27017)
    mongo_db_name       = kwargs.get('mongo_db_name', 'pim')
    graph_cache_time    = kwargs.get('graph_cache_time', 500)

    # Create flask application object
    app = Flask(__name__)

    # Configure flask application
    app.config['SWAGGER_UI_DOC_EXPANSION']  = 'list'
    app.config['RESTPLUS_VALIDATE']         = True
    app.config['RESTPLUS_MASK_SWAGGER']     = False
    app.config['ERROR_404_HELP']            = False

    # Create api blueprint
    api_bp = Blueprint('api', __name__, url_prefix='/api')

    # Initialize rest api blueprint
    api.init_app(api_bp)

    # Register blueprints
    app.register_blueprint(api_bp)
    app.register_blueprint(home_bp)

    # Set application logger
    app.log = logging.getLogger('app')

    # If the user selects the profiling option, then we need to do a little extra setup
    if flask_profile:
        from werkzeug.contrib.profiler import ProfilerMiddleware

        app.config['PROFILE']   = True
        app.wsgi_app            = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
        app.debug               = True

    # Flask debugging on/off
    app.debug = flask_debug

    # Setup database connection
    app.mongodb = MongoDB(host=mongo_host, port=mongo_port, db_name=mongo_db_name)

    # Set kgraph caching interval
    app.graph_caching_interval = graph_cache_time

    return app

def create_app_from_config(config=''):
    """Creates a PIM-Flask application from a template

    :param kwargs: PIM-Flask configuration name
    :return: Flask application
    """

    # Available configurations
    configs = ['development', 'testing', 'production']

    if not config in configs:
        raise Exception('{} not available!'.format(config))

    # Create the config parser
    config_parser = configparser.ConfigParser()

    # Get absolute PIM server directory name
    server_path = os.path.dirname(__file__)

    # Read from configuration file
    config_parser.read('{}/config/{}.cfg'.format(server_path, config))

    arguments = dict()

    # Establish MongoDB settings
    arguments['flask_debug']        = bool(config_parser.get('flask', 'debug'))
    arguments['flask_testing']      = bool(config_parser.get('flask', 'testing'))
    arguments['mongo_host']         = config_parser.get('mongo', 'host')
    arguments['mongo_port']         = int(config_parser.get('mongo', 'port'))
    arguments['mongo_db_name']      = config_parser.get('mongo', 'db_name')
    arguments['graph_cache_time']   = int(config_parser.get('pim', 'graph_cache_time'))

    return create_app(**arguments)


def main():
    """Stand-alone PIM application

    """

    # Set up the command-line arguments parser
    parser = argparse.ArgumentParser(description='PIM - Webserver')

    # Create Flask option group and options
    flask_group = parser.add_argument_group('Flask', 'Flask options')

    flask_group.add_argument('--flask_host', help='Hostname of the Flask app', default='0.0.0.0')
    flask_group.add_argument('--flask_port', type=int, help='Port for the Flask app ', default=5000)
    flask_group.add_argument('--flask_debug', action='store_true', dest='flask_debug', help='Flask debugging')
    flask_group.add_argument('--flask_profile', action='store_true', dest='flask_profile', help='Flask profiling')
    flask_group.add_argument('--flask_werkzeug_logging', action='store_true', dest='flask_werkzeug_logging', help='Flask Werkzeug logging')

    # Create MongoDB option group and options
    mongodb_group = parser.add_argument_group('MongoDB', 'MongoDB options')

    mongodb_group.add_argument('--mongo_host', dest='mongo_host', default='localhost', help='MongoDB host')
    mongodb_group.add_argument('--mongo_port', type=int, dest='mongo_port', default=27017, help='MongoDB port')
    mongodb_group.add_argument('--mongo_db_name', dest='mongo_db_name', default='pim', help='MongoDB database name')

    # Create PIM option group and add options
    pim_group = parser.add_argument_group('PIM', 'PIM options')

    pim_group.add_argument('--graph_cache_interval', type=int, dest='graph_cache_interval', default=500, help='Graph caching interval in milliseconds')

    # Parse the command line arguments
    arguments = parser.parse_args()

    # Create the flask application and set up the database
    app = create_app(**vars(arguments))

    # Enable/disable werkzeug logging
    if not arguments.flask_werkzeug_logging:
        werkzeug_logger.disabled = True

    # Run the Flask web server
    app.run(debug=arguments.flask_debug, host=arguments.flask_host, port=int(arguments.flask_port))

if __name__ == "__main__":
    main()