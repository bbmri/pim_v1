from flask import current_app
from flask_restplus import Resource

from server.api.models.run import run
from server.api.restplus import api

ns = api.namespace('runs', description='Operations related to runs')


@ns.route('/<string:run_id>')
@api.doc(params={'run_id': 'The run ID'})
@api.response(404, 'Run not found')
class RunItem(Resource):
    @api.marshal_with(run, 404, description='Run')
    def get(self, run_id):
        """
        Return run
        """

        if not current_app.mongodb.run_exists(run_id):
            return None, 404

        run_record = current_app.mongodb.run(run_id)

        return run_record['run']

    @api.response(200, 'Run successfully removed')
    @api.response(404, 'Run not found')
    def delete(self, run_id):
        """
        Delete run
        """

        if not current_app.mongodb.run_exists(run_id):
            return None, 404

        current_app.mongodb.delete_run(run_id)

        return None
