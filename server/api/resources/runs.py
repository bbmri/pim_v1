from flask import request, current_app, abort
from flask_restplus import Resource
from werkzeug.exceptions import BadRequest

from server.api.graph.sanity import check_run_sanity
from server.api.models.run import run
from server.api.restplus import api

ns = api.namespace('runs', description='Operations related to runs')


@ns.route('/')
class RunsCollection(Resource):
    # Override validate_payload method from Resource class
    def validate_payload(self, func):
        try:
            super(RunsCollection, self).validate_payload(func)
        except BadRequest as e:

            abort('asd')

    def get(self):
        """
        Return runs
        """

        runs = list()

        for run in current_app.mongodb.db.runs.find():
            runs.append(run['run']['id'])

        return runs

    @api.expect(run)
    @api.response(201, 'Run successfully created')
    @api.response(500, 'Run not created due to internal server error')
    def put(self):
        """
        Add or update run
        """

        # Get run JSON payload
        run_json = request.json

        # Do a sanity check of the run before we add it to the database
        sane, message = check_run_sanity(run_json)

        # Return bad request HTTP error code with clarifying message if the run is not sane
        if not sane:
            return message, 400

        current_app.mongodb.upsert_run(run_json)

        return 200, {'Location': 'api/runs/{}'.format(run.get('id'))}
