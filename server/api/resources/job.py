from flask import request, current_app
from flask_restplus import Resource

from server.api.models.job import job
from server.api.restplus import api

ns = api.namespace('runs', description='Operations related to runs')


@ns.route('/<string:run_id>/jobs/<string:job_id>')
@api.doc(params={'run_id': 'The run ID'})
@api.doc(params={'job_id': 'The job ID'})
class JobItem(Resource):
    @api.response(404, 'Run does not exist')
    @api.response(500, 'Internal server error')
    def get(self, run_id, job_id):
        """
        Return a job
        """

        if not current_app.mongodb.job_exists(run_id, job_id):
            return None, 404

        job_record = current_app.mongodb.job(run_id, job_id)

        return job_record['job']

    @api.expect(job)
    @api.response(201, 'Job successfully created/updated')
    @api.response(500, 'Internal server error')
    def put(self, run_id, job_id):
        """
        Add or update job
        """

        if not current_app.mongodb.run_exists(run_id):
            return None, 404

        current_app.mongodb.upsert_job(run_id, request.json)

        return 200, {'Location': 'api/runs/{}/jobs/{}'.format(run_id, job_id)}

    @api.response(200, 'Job successfully removed')
    @api.response(404, 'Job not removed because it does not exist')
    @api.response(500, 'Internal server error')
    def delete(self, run_id, job_id):
        """
        Add or update job
        """

        if not current_app.mongodb.job_exists(run_id, job_id):
            return None, 404

        current_app.mongodb.delete_job(run_id, job_id)

        return None
