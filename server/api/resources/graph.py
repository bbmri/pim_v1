import time, pprint

from flask import current_app
from flask_restplus import Resource

from server.api.graph.kgraph import create_network
from server.api.restplus import api

ns = api.namespace('runs', description='Operations related to runs')


@ns.route('/<string:run_id>/graph')
@api.doc(params={'run_id': 'The run ID'})
class GraphItem(Resource):
    def get(self, run_id):
        """
        Return graph
        """

        if not current_app.mongodb.run_exists(run_id):
            return None, 404

        # Whether or not a new graph should be updated
        graph_cache_invalid = False

        # Current time stamp
        now_ts = int(round(time.time() * 1000))

        graph = current_app.mongodb.graph(run_id)

        # If no graph is found, a new graph should be created
        if not graph:
            graph_cache_invalid = True
        else:
            graph_ts    = graph['last_modified']
            run_ts      = current_app.mongodb.run_ts(run_id)
            dt          = now_ts - graph_ts

            # If the interval exceeds the graph caching interval and the run was modified recently then invalidate the graph cache
            if dt > current_app.graph_caching_interval:
                if run_ts > graph_ts:
                    graph_cache_invalid = True

        if graph_cache_invalid:

            # Jobs belonging to the run
            jobs = current_app.mongodb.jobs(run_id)

            # Retrieve complete run from the db
            run = current_app.mongodb.run(run_id)['run']

            # And use the run together with the jobs to generate the kgraph
            graph_json, root_node = create_network(run, jobs)

            # Upsert the graph in the database
            current_app.mongodb.upsert_graph(run_id, graph_json)

        # Retrieve the graph from the db
        graph_record = current_app.mongodb.graph(run_id)

        return {'graph': graph_record['graph'], 'last_modified': graph_record['last_modified']}
