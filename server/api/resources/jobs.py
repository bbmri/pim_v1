from flask import current_app
from flask_restplus import Resource

from server.api.restplus import api

ns = api.namespace('runs', description='Operations related to runs')

@ns.route('/<string:run_id>/jobs')
@api.doc(params={'run_id': 'The run ID'})
class JobsItem(Resource):
    @api.response(404, 'Run does not exist')
    @api.response(500, 'Internal server error')
    def get(self, run_id):
        """
        Return run jobs
        """

        jobs = list()

        for job in current_app.mongodb.db.jobs.find({ 'job.run_id' : run_id }):
            jobs.append(job['job'])

        return jobs