import time, logging

from pymongo import MongoClient


class MongoDB(object):
    def __init__(self, **kwargs):
        """Initializes the MongoDB connection

        :param kwargs: Keyword arguments
        """

        super(MongoDB, self).__init__()

        self.host       = kwargs.get('host', 'localhost')
        self.port       = kwargs.get('port', 27017)
        self.db_name    = kwargs.get('db_name', 'pim')
        self.log        = logging.getLogger('mongodb')

        # Set up MongoDB client
        self.client = MongoClient(self.host, self.port, serverSelectionTimeoutMS=kwargs.get('mongo_timeout', 1000))

        # Check the database connection
        self.client.server_info()

        # Pick database
        self.db = self.client[self.db_name]

    def drop_database(self):
        """Drop the PIM database

        """
        
        self.client.drop_database(self.db_name)

    def now_ts(self):
        """Current timestamp

        :return: Current timestamp in milliseconds
        """

        return int(round(time.time() * 1000))

    def reset(self):
        """Reset the entire database"""

        self.db.runs.delete_many({})
        self.db.jobs.delete_many({})
        self.db.graphs.delete_many({})

    def run(self, run_id):
        """Fetch run from database

        :param run_id: Run ID
        :return: Run document (JSON)
        """

        run_record = self.db.runs.find_one({'run.id': run_id})

        if run_record:
            return run_record
        else:
            return None

    def run_ts(self, run_id):
        """Fetch run time stamp from database

        :param run_id: Run ID
        :return: Run time stamp
        """

        run_record = self.db.runs.find_one({'run.id': run_id}, {'last_modified': 1})

        return run_record['last_modified']

    def run_exists(self, run_id):
        """Determine if a run exists in the database

        :param run_id: Run ID
        :return: Boolean indicating whether the run exists
        """

        run_record = self.db.runs.find_one({'run.id': run_id}, {'id': 1})

        if run_record:
            return True
        else:
            return False

    def upsert_run(self, run):
        """Inserts/updates a run document in the database

        :param run: Run (JSON)
        """

        # Create run document
        run_doc = {'run': run, 'last_modified': self.now_ts()}

        # Insert/update the run document
        self.db.runs.update({'run.id': run['id']}, run_doc, upsert=True)

        # Remove all existing jobs and graphs for this run
        self.db.jobs.delete_many({'job.run_id': run['id']})
        self.db.graphs.delete_many({'run_id': run['id']})

    def delete_run(self, run_id):
        """Removes a run from the database

        :param run_id: Run ID
        """

        self.db.runs.delete_one({'run.id': run_id})

        # Remove all jobs and graphs that are associated with the run
        self.db.jobs.remove({'job.run_id': run_id})
        self.db.graphs.remove({'run_id': run_id})

    def set_run_modified(self, run_id):
        """Adjusts the last_modified timestamp in the run document (used in the caching mechanism)

        :param run_id: Run ID
        """

        # Update the time stamp in the run document
        self.db.runs.update({'run.id': run_id}, {'$set': {'last_modified': self.now_ts()}})

    def job(self, run_id, job_id):
        """Fetch job from database

        :param run_id: Run ID
        :param job_id: Job ID
        :return: Job document (JSON)
        """

        job_record = self.db.jobs.find_one({'job.run_id': run_id, 'job.id': job_id})

        if job_record:
            return job_record
        else:
            return None

    def job_exists(self, run_id, job_id):
        """Determine if a job exists in the database

        :param run_id: Run ID
        :param job_id: Job ID
        :return: Boolean indicating whether the job exists
        """

        job_record = self.db.jobs.find_one({'job.run_id': run_id, 'job.id': job_id}, {'id': 1})

        if job_record:
            return True
        else:
            return False

    def upsert_job(self, run_id, job):
        """Inserts/updates a job document in the database

        :param run_id: Run ID
        :param job: Job (JSON)
        """

        # Create job document
        job_doc = {'job': job}

        existing_job = self.db.jobs.find_one({'job.run_id': run_id, 'job.id': job['id']})

        if existing_job:
            # print('{} status from: {} to {}'.format(job['id'], existing_job['job']['status'], job['status']))

            if existing_job['job']['status'] == 'success' and job['status'] == 'failed':
                print('============ERROR============')
        # else:
        #     print('{} status: {}'.format(job['id'], job['status']))

        # Insert/update the job document in the database
        self.db.jobs.update({'job.run_id': run_id, 'job.id': job['id']}, job_doc, upsert=True)

        # A job has changed so the run is modified
        self.set_run_modified(run_id)

    def delete_job(self, run_id, job_id):
        """Removes a job from the database

        :param run_id: Run ID
        :param job_id: Job ID
        """

        # Delete the job from the database
        self.db.jobs.delete_one({'job.run_id': run_id, 'job.id': job_id})

        # A job has changed so the run is modified
        self.set_run_modified(run_id)

    def jobs(self, run_id):
        """Fetches all jobs for a specific run

        :param run_id:
        :return: List of jobs
        """

        jobs = list()

        for job in self.db.jobs.find({'job.run_id': run_id}):
            jobs.append(job['job'])

        return jobs

    def graph(self, run_id):
        """Fetch graph from database

        :param run_id: Run ID
        :return: Graph document (JSON)
        """

        # Get graph record from the database
        graph_record = self.db.graphs.find_one({'run_id': run_id})

        if graph_record:
            return graph_record
        else:
            return None

    def graph_exists(self, run_id):
        """Determine if a graph exists in the database

        :param run_id: Run ID
        :return: Boolean indicating whether the graph exists
        """

        # Get graph record from the database
        graph_record = self.db.graphs.find_one({'run_id': run_id}, {'last_modified': 1})

        if graph_record:
            return True
        else:
            return False

    def upsert_graph(self, run_id, graph):
        """Inserts/updates a graph document in the database

        :param run_id: Run ID
        :param graph: Graph (JSON)
        """

        # Create graph record
        graph_record = {'run_id': run_id, 'graph': graph, 'last_modified': self.now_ts()}

        # Write the graph to the database
        self.db.graphs.update({'run_id': run_id}, graph_record, upsert=True)

    def graph_ts(self, run_id):
        """Fetch graph time stamp from database

        :param run_id: Run ID
        :return: Graph time stamp
        """

        graph_record = self.db.graphs.find_one({'run_id': run_id}, {'last_modified': 1})

        return graph_record['last_modified']