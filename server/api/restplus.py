import logging

from flask_restplus import Api

log = logging.getLogger(__name__)

# Create the API
api = Api(version='1.0', title='PIM API',  description='API documentation for the Pipeline Inspection and Monitoring web micro service ', doc="/documentation")