from flask_restplus import fields

from server.api.restplus import api

port = api.model('Port', {
    'id': fields.String(required=True, description='Port identifier', default='undefined'),
    'description': fields.String(required=True, description='Port description', default='undefined')
})