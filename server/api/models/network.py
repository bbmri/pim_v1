from flask_restplus import fields

from server.api.models.group import group
from server.api.models.link import link
from server.api.models.node import node
from server.api.restplus import api

network = api.model('Network', {
    'description': fields.String(required=True, description='Network description', default='undefined'),
    'nodes': fields.List(fields.Nested(node)),
    'links': fields.List(fields.Nested(link)),
    'groups': fields.List(fields.Nested(group), default=[{ 'id' : 'root', 'parent_group' : '', 'description' : 'Root of all node groups' }])
})
