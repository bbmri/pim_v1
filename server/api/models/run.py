from flask_restplus import fields

from server.api.models.network import network
from server.api.restplus import api

run = api.model('Run', {
    'id': fields.String(required=True, description='Run identifier', default='undefined'),
    'description': fields.String(required=True, description='Run description', default='undefined'),
    'network': fields.Nested(network),
    'collapse': fields.Boolean(default=False, description='Whether to collapse all node in/out ports to single node in/out port'),
    'workflow_engine': fields.String(required=True, description='Type of workflow engine')
})