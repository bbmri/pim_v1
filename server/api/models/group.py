from flask_restplus import fields

from server.api.restplus import api

group = api.model('Group', {
    'id': fields.String(required=True, description='Group ID', pattern=''),
    'parent_group': fields.String(required=True, description='Parent group id', default='root'),
    'description': fields.String(required=False, description='Group description', default='No description')
})