from flask_restplus import fields

from server.api.models.port import port
from server.api.restplus import api

node = api.model('Node', {
    'id': fields.String(required=True, description='Node ID', pattern=''),
    'type': fields.String(required=True, description='Node type'),
    'in_ports': fields.List(fields.Nested(port)),
    'out_ports': fields.List(fields.Nested(port)),
    'group_id': fields.String(required=False, description='The group id this node belongs to', default='root')
})