from flask_restplus import fields

from server.api.restplus import api

job = api.model('Job', {
    'id': fields.String(required=True, description='Job ID', default='undefined'),
    'run_id': fields.String(required=True, description='Run ID', default='undefined'),
    'node_id': fields.String(required=True, description='Node ID', default='undefined'),
    'sample_id': fields.String(required=True, description='Sample ID', default='undefined'),
    'status': fields.String(required=True, enum=['idle', 'running', 'success', 'failed'], description='Job status')
})