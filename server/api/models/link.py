from flask_restplus import fields

from server.api.restplus import api

link = api.model('Link', {
    'id': fields.String(required=True, description='Link ID', default='undefined'),
    'from_node': fields.String(required=True, description='From node', default='undefined'),
    'from_port': fields.String(required=True, description='From port', default='undefined'),
    'to_node': fields.String(required=True, description='To node', default='undefined'),
    'to_port': fields.String(required=True, description='To port', default='undefined'),
    'type': fields.String(required=False, description='Data type associated with this link', default=''),
})