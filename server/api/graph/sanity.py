
def check_invalid_group_id(run):
    """Check if one or more nodes references a non-existing group

    :param run: Run to check
    """

    network = run['network']

    # List groups
    groups = [group['id'] for group in network['groups']]

    # Root group always exists
    groups.append('root')

    # Check if nodes have an invalid reference to a group
    for node in network['nodes']:
        if node['group_id'] not in groups:
            raise Exception("Node '{}' is member of non-existing group: '{}')".format(node['id'], node['group_id']))


def check_invalid_parent_group(run):
    """Check if a group references a non-existing parent group

    :param run: Run to check
    """

    network = run['network']

    # List groups
    groups = [group['id'] for group in network['groups']]

    # Root group always exists
    groups.append('root')

    # For each group in the network check if the parent group exists
    for group in network['groups']:
        if group['parent_group'] not in groups:
            raise Exception("Group '{}' is child of non-existing parent group: '{}')".format(group['id'], group['parent_group']))


def check_portless_nodes(run):
    """Check if there are any nodes that have no ports at all

    :param run: Run to check
    """

    network = run['network']

    # For each node in the network check if it has ports
    for node in network['nodes']:
        if len(node['in_ports']) == 0 and len(node['out_ports']) == 0:
            raise Exception("Node '{}' has no input/output ports".format(node['id']))


def check_empty_group(run):
    """Check the run for empty groups

    :param run: Run to check
    """

    network = run['network']

    # List groups
    groups = [group['id'] for group in network['groups']]

    # Root group always exists
    groups.append('root')

    def is_leaf_group(group_id):
        for group in network['groups']:
            if group['parent_group'] == group_id:
                return False

        return True

    for group in groups:
        if not is_leaf_group(group):
            continue

        group_nodes = list()

        for node in network['nodes']:
            if node['group_id'] == group:
                group_nodes.append(node)

        if len(group_nodes) == 0:
            raise Exception("Group '{}' is empty)".format(group))


def check_links(run):
    """Check link sanity

    :param run: Run to check
    """

    network = run['network']

    # Node dictionary for easy check
    nodes = dict()

    # Enumerate nodes
    for node in network['nodes']:
        nodes[node['id']] = node

    # Check validity of each link
    for link in network['links']:
        if not link['from_node'] in nodes.keys():
            raise Exception("Link '{}' invalid because from node '{}' does not exist".format(link['id'], link['from_node']))

        if not link['to_node'] in nodes.keys():
            raise Exception("Link '{}' invalid because to node '{}' does not exist".format(link['id'], link['from_node']))

        from_node   = nodes[link['from_node']]
        to_node     = nodes[link['to_node']]

        # Check if the 'from' port exists on the from node
        if not any(out_port['id'] == link['from_port'] for out_port in from_node['out_ports']):
            raise Exception("Link '{}' invalid because from port '{}' does not exist on '{}'".format(link['id'], link['from_port'], link['from_node']))

        # Check if the 'to' port exists on the to node
        if not any(in_port['id'] == link['to_port'] for in_port in to_node['in_ports']):
            raise Exception("Link '{}' invalid because to port '{}' does not exist on '{}'".format(link['id'], link['to_port'], link['to_node']))


def check_duplicate_group_ids(run):
    """Check the run for duplicate group ID's

    :param run: Run to check
    """

    network = run['network']

    # List group id's
    group_ids = [group['id'] for group in network['groups']]

    # Find duplicates
    duplicates = set([group_id for group_id in group_ids if group_ids.count(group_id) > 1])

    # Raise exception if we found duplicates
    if len(duplicates) > 0:
        raise Exception("Duplicate group ID's: {})".format(list(duplicates)))


def check_duplicate_node_ids(run):
    """Check the run for duplicate node ID's

    :param run: Run to check
    """

    network = run['network']

    # List group id's
    node_ids = [node['id'] for node in network['nodes']]

    # Find duplicates
    duplicates = set([node_id for node_id in node_ids if node_ids.count(node_id) > 1])

    # Raise exception if we found duplicates
    if len(duplicates) > 0:
        raise Exception("Duplicate node ID's: {})".format(list(duplicates)))


def check_run_sanity(run):
    """Check the run sanity

    :param run: Run to check
    :return: Run sanity true/false
    """
    try:
        check_invalid_group_id(run)
        check_invalid_parent_group(run)
        check_portless_nodes(run)
        check_empty_group(run)
        check_links(run)
        check_duplicate_group_ids(run)
        check_duplicate_node_ids(run)

    except Exception as e:
        return False, str(e)

    return True, ''