import random


class GraphNodeStatus(object):
    def __init__(self, **kwargs):
        """Initializes the graph node status object

        :param kwargs: Keyword arguments
        """

        super(GraphNodeStatus, self).__init__()

        self.progress   = kwargs.get('progress', 0)
        self.result     = 'pending'
        self.no_jobs    = 0
        self.jobs       = dict()
        self.state      = 'pending'

        self.jobs['idle']       = kwargs.get('idle', 0)
        self.jobs['running']    = kwargs.get('running', 0)
        self.jobs['success']    = kwargs.get('success', 0)
        self.jobs['failed']     = kwargs.get('failed', 0)

    def reset(self):
        """Reset the status

        """

        self.progress   = 0
        self.no_jobs    = 0

        for key in self.jobs.keys():
            self.jobs[key] = 0

    def randomize_jobs(self):
        """Randomize the job status (for testing purposes only)

        """

        if random.random() > 0.5:
            for key in self.jobs.keys():
                self.jobs[key] = random.randint(1, 12)

        self.compute_progress()

    def add_job(self, type):
        """Add a job to the status

        :param type: Type of job
        """

        self.jobs[type] += 1

        self.compute_progress()

    def compute_progress(self):
        """Compute progress (percentages etc.)

        """

        self.compute_no_jobs()

        self.progress = 0

        if self.no_jobs > 0:
            self.progress = (self.jobs['success'] + self.jobs['failed']) / self.no_jobs

        self.state = 'pending'

        if self.progress == 1 and self.jobs['failed'] > 0:
            self.state = 'failed'

        if self.progress == 1 and self.jobs['failed'] == 0 and self.jobs['success'] == self.no_jobs:
            self.state = 'success'

    def compute_no_jobs(self):
        """Compute the total number of jobs

        """

        self.no_jobs = sum(self.jobs.values())

    def __add__(self, other):
        """Add two graph node status objects

        :param other: Other GraphNodeStatus to add
        :return:
        """

        # Resulting graph status
        result = GraphNodeStatus()

        # Key wise addition of jobs
        for key in self.jobs.keys():
            result.jobs[key] = self.jobs[key] + other.jobs[key]

        result.compute_progress()

        return result

    def to_kgraph(self):
        """Convert to K-graph representation

        :return:
        """

        status = dict()

        status['progress']  = self.progress
        status['state']     = self.state
        status['no_jobs']   = self.no_jobs
        status['jobs']      = list()

        for job, count in self.jobs.items():
            percentage = 0

            # Prevent zero division
            if self.no_jobs > 0:
                percentage = count / self.no_jobs

            status['jobs'].append(dict(name=job, value=count, percentage=percentage))

        return status


class GraphNode(object):
    def __init__(self, **kwargs):
        """Initializes the graph node

        :param kwargs: Keyword arguments
        """

        super(GraphNode, self).__init__()

        self.id             = kwargs.get('id', '')
        self.level          = kwargs.get('level', 0)
        self.type           = kwargs.get('type', 'leaf')
        self.group_id       = kwargs.get('group_id', '')
        self.parent         = kwargs.get('parent', None)
        self.leaf           = kwargs.get('leaf', None)
        self.children       = list()
        self.links          = list()
        self.status         = GraphNodeStatus()
        self.propagate_up   = True

    def add_leaves(self, leaves):
        """Add leaf graph nodes

        :param leaves: Leafs
        """

        for leaf in leaves:
            if self.type is 'group' and leaf['group_id'] == self.group_id:

                # The leaf belongs to this group so create a leave node for it
                leaf_node = GraphNode(id=leaf['id'], group_id=self.group_id, parent=self, leaf=leaf)

                # Add it to our list of children
                self.children.append(leaf_node)

                # And remove the leaf from the list for optimization
                # leafs.remove(leaf)

        # Add leafs to child groups
        for child in self.children:
            if child.type is 'group':
                child.add_leaves(leaves)

    def add_jobs(self, jobs):
        """Add jobs to graph node

        :param jobs: List of jobs
        """

        self.status.reset()

        for job in jobs:
            if job['node_id'] == self.id:
                self.status.add_job(job['status'])

        for child in self.children:
            child.add_jobs(jobs)

    def children_by_type(self, node_type):
        """Get children by type

        :param node_type: Node type (group/leaf)
        :return: Children of type
        """

        result = list()

        for child in self.children:
            if child.type == node_type:
                result.append(child)

        return result

    def has_child_groups(self):
        """If the graph node has child groups

        :return: Has child groups true/false
        """

        return len(self.children_by_type('group')) > 0

    def is_group_leaf(self):
        """Determine if this node is a leaf and a group

        :return: Is group and leaf (true/false)
        """

        return self.type == 'group' and not self.has_child_groups()

    def compute_status(self):
        """Compute graph node status

        """

        self.group_progress()

    def group_progress(self):
        """Compute the group node progress by combining the progress of child nodes

        """

        # Compute group statuses
        if self.type == 'group':
            child_leafs = self.children_by_type('leaf')

            for child_leaf in child_leafs:
                self.status += child_leaf.status

        for child_group in self.children_by_type('group'):
            child_group.group_progress()

    def propagate_status_up(self):
        """Recursively add the status upward

        """

        group_leafs = self.group_leafs()

        for group_leaf in group_leafs:
            group_leaf.parent.status += group_leaf.status

    def group_leafs(self):
        """Determine which nodes are leaf groups

        Usually executed on the root node.

        :return:
        """

        if not self.parent:
            leafs = list()

            for child in self.children:
                leafs.extend(child.group_leafs())

            return leafs
        else:
            if self.is_group_leaf():
                return [self]
            else:
                leafs = list()

                for child in self.children:
                    leafs.extend(child.group_leafs())

                return leafs

    def group_to_kgraph(self, node):
        """Convert group to K-graph representation

        :param node: Input node
        """

        node['padding'] = dict(left=5, top=60, right=5, bottom=5)

    def leaf_to_kgraph(self, node):
        """Convert group to K-graph representation

        :param node: Input node
        """

        node['padding'] = dict(left=5, top=5, right=5, bottom=5)
        node['width']   = 200
        node['height']  = 100
        node['ports']   = list()
        node['type']    = self.leaf['type']

        def create_port(port, side):
            id          = self.leaf['id'] + '_' + port['id']
            properties  = dict(portSide=side, portSpacing=1)

            return dict(id=id, properties=properties)

        for in_port in self.leaf['in_ports']:
            node['ports'].append(create_port(in_port, 'WEST'))

        for out_port in self.leaf['out_ports']:
            node['ports'].append(create_port(out_port, 'EAST'))

        node['properties'] = dict(portConstraints='FIXED_SIDE', portSpacing=20)

    def to_kgraph(self):
        """Convert node to K-graph representation

        :return:
        """
        node = dict()

        node['id']      = self.id
        node['level']   = self.level
        node['ports']   = list()
        node['edges']   = list()

        if len(self.links) > 0:
            node['edges'] = list()

            for link in self.links:
                source      = link['from_node']
                source_port = link['from_node'] + '_' + link['from_port']
                target      = link['to_node']
                target_port = link['to_node'] + '_' + link['to_port']
                id          = source_port + "_" + target_port

                node['edges'].append(dict(id=id, source=source, sourcePort=source_port, target=target, targetPort=target_port, type=link['type']))

        if self.type == 'group':
            self.group_to_kgraph(node)
        else:
            self.leaf_to_kgraph(node)

        node['status'] = self.status.to_kgraph()

        if len(self.children) > 0:
            node['children'] = list()

            for child in self.children:
                node['children'].append(child.to_kgraph())

        # import json
        #
        # if self.id == 'root':
        #     print(self.status.jobs['failed'])

        return node

    def randomize_status(self):
        """Randomize the status of the node

        Only for testing purposes
        """

        if self.type == 'leaf':
            self.status.randomize_jobs()

        for child in self.children:
            child.randomize_status()

class GraphGroup(object):
    def __init__(self, id, **kwargs):
        """Initializes the graph group

        :param id: Group ID
        :param kwargs: Keyword arguments
        """

        super(GraphGroup, self).__init__()

        self.id         = id
        self.children   = list()

        for group in kwargs.get('groups'):
            if group['parent_group'] == self.id:
                self.children.append(GraphGroup(id=group['id'], **kwargs))

    def build_node_tree(self, parent_node=None):
        """Build node tree

        :param parent_node: Parent node
        :return:
        """

        if parent_node is None:
            root_node = GraphNode(id='root', type='group', group_id='root')

            for child in self.children:
                child.build_node_tree(parent_node=root_node)

            return root_node
        else:
            # Create new node for this group
            node = GraphNode(id=self.id, level=parent_node.level + 1, type='group', group_id=self.id, parent=parent_node)

            # Add it to our parent node
            parent_node.children.append(node)

            # And add our child groups as well
            for child in self.children:
                child.build_node_tree(parent_node=node)


def create_network(run, jobs):
    """Create K-graph from run specs and job list

    :param run: Run specification
    :param jobs: Job list
    :return: K-graph
    """

    network = run['network']

    # Deduce group hierarchy
    root_group = GraphGroup(id='root', groups=run['network']['groups'])

    # And convert the GraphGroup hierarchy to a GraphNode hierarchy
    root_node = root_group.build_node_tree()

    root_node.add_leaves(network['nodes'])
    root_node.add_jobs(jobs)

    root_node.links = network['links']

    # root_node.randomize_status()
    root_node.compute_status()
    root_node.propagate_status_up()

    return root_node.to_kgraph(), root_node