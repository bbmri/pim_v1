from flask import Blueprint, render_template, current_app, redirect

home_bp = Blueprint('home', __name__, url_prefix='')


@home_bp.route('/')
@home_bp.route('/runs')
def index():
    run_ids = list()

    for run in current_app.mongodb.db.runs.find():
        run_ids.append(run['run']['id'])

    return render_template('runs.html', run_ids=run_ids)


@home_bp.route('/run/<run_id>')
def run(run_id):
    return render_template('run.html', run_id=run_id)


@home_bp.route('/jobs/<run_id>')
def jobs(run_id):
    jobs = list()

    for job in current_app.mongodb.db.jobs.find({'job.run_id': run_id}):
        jobs.append(job['job'])

    return render_template('jobs.html', run_id=run_id, jobs=jobs)


@home_bp.route('/reset_db')
def reset_db():
    current_app.mongodb.reset()

    return redirect('/runs')


@home_bp.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@home_bp.errorhandler(500)
def internal_error(error):
    return render_template('500.html'), 500
