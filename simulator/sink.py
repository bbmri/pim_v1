from simulator.node import Node


class Sink(Node):
    """Convenience class which models a sink node (consisting of only one input port)

    """

    def __init__(self, **kwargs):

        # Sink node has only one input port
        kwargs['no_in_ports']   = 1
        kwargs['no_out_ports']  = 0

        super(Sink, self).__init__(**kwargs)

        self.type = 'sink'
