class Signal(object):
    """Subject observer pattern class

    """

    def __init__(self, sender):
        super(Signal).__init__()

        self._handlers  = []
        self.sender     = sender

    def connect(self, handler):
        """Connect to a subject

        :param handler: Handler subject
        """

        self._handlers.append(handler)

    def fire(self, *args, **kwargs):
        """Broadcast message to receivers

        :param args: Arguments
        :param kwargs: Keyword arguments
        """
        for handler in self._handlers:
            handler(self.sender, *args, **kwargs)
