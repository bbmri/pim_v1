from simulator.log import logger
from simulator.signal import Signal


class Link:
    """Class that facilitates data flow connections between node ports

    """

    def __init__(self, from_node, from_port, to_node, to_port):
        self.id             = '{}@{} > {}@{}'.format(from_node.id, from_port.id, to_node.id, to_port.id)
        self.from_node      = from_node
        self.from_port      = from_port
        self.to_node        = to_node
        self.to_port        = to_port
        self.link_type      = "default"
        self.finished       = Signal(self)

        self.from_port.connect(self)
        self.to_port.connect(self)

        # Inform us when the state input port finishes
        self.from_port.finished.connect(self.on_input_finished)

    def on_input_finished(self, input_port):
        """Callback when connected input node is finished with processing

        :param input_port: Connected input port
        """
        logger.debug('Link {0}:{1} finished'.format(self.from_node.id, self.from_port.id))

        self.finished.fire()

    @staticmethod
    def connect(network, node_a, node_b, port_id_a=-1, port_id_b=-1):
        """Connect two ports from two nodes

        :param network: Input network
        :param node_a: Input node A
        :param node_b: Input node B
        :param port_id_a: Input port A (from node A)
        :param port_id_b: Input port B (from node B)
        """

        port_a = node_a.out_ports[port_id_a]
        port_b = node_b.in_ports[port_id_b]

        if not port_a or not port_b:
            raise Exception("{0}:{1}".format(port_a, port_b))

        network.links.append(Link(node_a, port_a, node_b, port_b))

    def from_node_id(self):
        """Get the left node of the connection

        :return: From node
        """

        if self.from_node:
            return self.from_node.id

        return ''

    def from_port_id(self):
        """Get the left port of the connection

        :return: From port
        """

        if self.from_port:
            return self.from_port.id

        return ''

    def to_node_id(self):
        """Get the right node of the connection

        :return: To node
        """

        if self.to_node:
            return self.to_node.id

        return ''

    def to_port_id(self):
        """Get the right port of the connection

        :return: To port
        """

        if self.to_port:
            return self.to_port.id

        return ''

    def __getstate__(self):
        return dict(id=self.id, from_node=self.from_node_id(), from_port=self.from_port_id(), to_node=self.to_node_id(),
                    to_port=self.to_port_id(), type=self.link_type)

    def __repr__(self):
        return str(self.__dict__)
