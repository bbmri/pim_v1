from simulator.constant import Constant
from simulator.link import Link
from simulator.node import Node
from simulator.run import Run
from simulator.sink import Sink
from simulator.source import Source


def create_run(**kwargs):
    """Create example run with a basic network

    :param kwargs: Keyword arguments
    :return: Run
    """

    kwargs['id']                = __name__.split('.')[-1] + '_test'
    kwargs['description']       = 'Minimal example'
    kwargs['workflow_engine']   = 'undefined'

    # Create run
    run = Run(**kwargs)

    # Get network
    network = run.network

    # Create nodes
    source      = network.add_node(Source, id='source')
    process     = network.add_node(Node, id='process', no_in_ports=4)
    constant    = network.add_node(Constant, id='constant')
    sink        = network.add_node(Sink, id='sink')

    # And connect them
    Link.connect(network, source, process, 0, 0)
    Link.connect(network, constant, process, 0, 1)
    Link.connect(network, process, sink)

    return run
