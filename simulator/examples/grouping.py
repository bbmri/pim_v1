from simulator.constant import Constant
from simulator.group import Group
from simulator.link import Link
from simulator.node import Node
from simulator.run import Run
from simulator.sink import Sink
from simulator.source import Source


def create_run(**kwargs):
    """Create example run that demonstrates the concept of grouping

    :param kwargs: Keyword arguments
    :return: Run
    """

    kwargs['id']                = __name__.split('.')[-1] + '_test'
    kwargs['description']       = 'Grouping example'
    kwargs['workflow_engine']   = 'undefined'

    # Create run
    run = Run(**kwargs)

    # Get network
    network = run.network

    # Add the groups to the network
    network.groups.append(Group(id='preprocessing', parent_group='root', description='Pre-processing steps'))
    network.groups.append(Group(id='processing', parent_group='root', description='Processing steps'))
    network.groups.append(Group(id='registration', parent_group='processing', description='Registration steps'))
    network.groups.append(Group(id='sinks', parent_group='root', description='All sinks'))
    network.groups.append(Group(id='segmentation', parent_group='processing', description='Segmentation steps'))

    # Create preprocesing nodes
    source              = network.add_node(Source, id='source_image', group_id='preprocessing', )
    bias_correction     = network.add_node(Node, id='bias_correction', no_in_ports=4, group_id='preprocessing')
    bias_param_a        = network.add_node(Constant, id='bias_param_a', group_id='preprocessing')
    bias_param_b        = network.add_node(Constant, id='bias_param_b', group_id='preprocessing')

    # Connect nodes
    Link.connect(network, source, bias_correction, 0, 0)
    Link.connect(network, bias_param_a, bias_correction, 0, 1)
    Link.connect(network, bias_param_b, bias_correction, 0, 2)

    # Create registration nodes
    elastix             = network.add_node(Node, id='elastix', group_id='registration')
    elastix_param_a     = network.add_node(Constant, id='elastix_param_a', group_id='registration')
    elastix_param_b     = network.add_node(Constant, id='elastix_param_b', group_id='registration')
    transformix         = network.add_node(Node, id='transformix', group_id='registration')
    transformix_param_a = network.add_node(Constant, id='transformix_param_a', group_id='registration')
    transformix_param_b = network.add_node(Constant, id='transformix_param_b', group_id='registration')

    # Connect nodes
    Link.connect(network, bias_correction, elastix, 0, 0)
    Link.connect(network, elastix_param_a, elastix, 0, 1)
    Link.connect(network, elastix_param_b, elastix, 0, 2)
    Link.connect(network, elastix, transformix, 0, 0)
    Link.connect(network, transformix_param_a, transformix, 0, 1)
    Link.connect(network, transformix_param_b, transformix, 0, 3)

    # Create segmentation nodes
    segmentation            = network.add_node(Node, id='ws_segmentation', group_id='segmentation', no_out_ports=1)
    segmentation_param_a    = network.add_node(Constant, id='segmentation_param_a', group_id='segmentation', no_samples=1)
    segmentation_param_b    = network.add_node(Constant, id='segmentation_param_b', group_id='segmentation', no_samples=1)

    # Connect nodes
    Link.connect(network, segmentation_param_a, segmentation, 0, 1)
    Link.connect(network, segmentation_param_b, segmentation, 0, 2)
    Link.connect(network, bias_correction, segmentation, 0, 0)

    # Create sinks
    bias_correction_sink    = network.add_node(Sink, id='bias_correction_sink', group_id='sinks')
    elastix_sink            = network.add_node(Sink, id='elastix_sink', group_id='sinks')
    transformix_sink        = network.add_node(Sink, id='transformix_sink', group_id='sinks')
    segmentation_sink       = network.add_node(Sink, id='segmentation_sink', group_id='sinks')

    # Connect nodes
    Link.connect(network, bias_correction, bias_correction_sink)
    Link.connect(network, elastix, elastix_sink)
    Link.connect(network, transformix, transformix_sink)
    Link.connect(network, segmentation, segmentation_sink)

    return run
