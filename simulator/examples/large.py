import os

from simulator.group import Group
from simulator.constant import Constant
from simulator.link import Link
from simulator.node import Node
from simulator.run import Run
from simulator.sink import Sink
from simulator.source import Source


def create_run(**kwargs):
    """Create example run consisting of a large amount of connected nodes

    :param kwargs: Keyword arguments
    :return: Run
    """

    kwargs['id']                = __name__.split('.')[-1] + '_test'
    kwargs['description']       = 'Large example'
    kwargs['workflow_engine']   = 'undefined'

    os.environ['NO_SAMPLES'] = str(2)
    os.environ['JOB_MIN_DURATION'] = str(1)
    os.environ['JOB_MAX_DURATION'] = str(5)

    # Create run
    run = Run(**kwargs)

    # Get network
    network = run.network

    # Create nodes
    source = network.add_node(Source, id='source')

    for pid in range(10):
        group_name = 'group_{}'.format(pid)

        network.groups.append(Group(id=group_name, parent_group='root', description='Process {}'.format(pid)))

        constant_a  = network.add_node(Constant, id=group_name + '_constant_a', group_id=group_name)
        constant_b  = network.add_node(Constant, id=group_name + '_constant_b', group_id=group_name)
        process     = network.add_node(Node, id='process_{}'.format(pid), group_id=group_name, no_in_ports=4)
        sink        = network.add_node(Sink, id='sink_{}'.format(pid), group_id=group_name)

        # And connect them
        Link.connect(network, source, process, 0, 0)
        Link.connect(network, constant_a, process)
        Link.connect(network, constant_b, process)
        Link.connect(network, process, sink)

    return run
