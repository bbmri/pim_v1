import logging

logging.basicConfig(format='%(asctime)-15s - %(levelname)s - %(message)s', level=logging.DEBUG,
                    datefmt='%m/%d/%Y %I:%M:%S %p')

logger = logging.getLogger('tcpserver')
logger.disabled = True

logging.getLogger("requests").setLevel(logging.WARNING)