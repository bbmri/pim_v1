from simulator.node import Node


class Source(Node):
    """Convenience class which models a source node (consisting of only one output port)

    """

    def __init__(self, **kwargs):

        # Source node has only one output port
        kwargs['no_in_ports']   = 0
        kwargs['no_out_ports']  = 1

        super(Source, self).__init__(**kwargs)

        self.type = 'source'
