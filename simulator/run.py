import json
import jsonpickle
import requests
import os

from simulator.network import Network
from simulator.log import logger


class Run:
    """The run class represents an instance of a pipeline

    """

    def __init__(self, **kwargs):
        self.id                 = kwargs.get('id', 'undefined')
        self.description        = kwargs.get('description', 'No description yet')
        self.workflow_engine    = kwargs.get('workflow_engine', 'undefined')
        self.network            = Network(run_id=self.id)

    def pim_add_run(self):
        """Add this run to the pim server

        """

        payload = jsonpickle.encode(self, unpicklable=False)

        logger.debug('PIM add run {run_id}'.format(run_id=self.id))

        # Execute an http put request to add the run
        return requests.put(os.environ.get('PIM_SERVER_URL', 'http://localhost:5000') + '/api/runs/', json=json.loads(payload))

    def pim_remove_run(self):
        """Remove this run from the pim server

        """

        logger.debug('PIM remove run {run_id}'.format(run_id=self.id))

        # Execute an http delete request to remove the run
        return requests.delete(os.environ.get('PIM_SERVER_URL', 'http://localhost:5000') + '/api/runs/' + self.id)

    def pim_simulate(self):
        """Simulate this run as though it is executed in the context of a HPC cluster

        """

        logger.debug('PIM simulate run {run_id}'.format(run_id=self.id))

        self.pim_add_run()

        # Initiate the network simulation
        self.network.simulate()

    def to_json(self):
        """Convert run to JSON representation

        :return: JSON encoded run
        """

        return json.loads(jsonpickle.encode(self, unpicklable=False))

    def __getstate__(self):
        return dict(id=self.id, description=self.description, network=self.network,
                    workflow_engine=self.workflow_engine)

    def __repr__(self):
        return str(self.__dict__)
