class Group:
    """A container for nodes

    """

    def __init__(self, **kwargs):
        """Initialize the group"""

        self.id             = kwargs.get('id', 'root')
        self.parent_group   = kwargs.get('parent_group', 'root')
        self.description    = kwargs.get('description', 'No description')

    def __getstate__(self):
        return dict(id=self.id, parent_group=self.parent_group, description=self.description)

    def __repr__(self):
        return str(self.__dict__)
