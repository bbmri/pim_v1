import asyncio
import json
import jsonpickle
import random
import requests
import os

from simulator.log import logger


class Job:
    """Class which models job behaviour

     This class simulates a-synchronous jobs running on a cluster using the python asyncio package

    """

    def __init__(self, **kwargs):

        # Obtain job minimum and maximum duration from process environment
        job_min_duration = float(os.environ.get('JOB_MIN_DURATION', 500))
        job_max_duration = float(os.environ.get('JOB_MAX_DURATION', 1000))

        self.run_id                 = kwargs.get('run_id', 'undefined')
        self.node_id                = kwargs.get('node_id', 'undefined')
        self.sample_id              = kwargs.get('sample_id', 'undefined')
        self.id                     = '{}:{}'.format(self.node_id, self.sample_id)
        self.status                 = kwargs.get('status', 'idle')
        self.duration_range         = [job_min_duration, job_max_duration]
        self.failed_probability     = kwargs.get('failed_probability', 0.05)

    def put(self):
        """Dispatch job to PIM server

        """

        # Compose url for run
        url = '{pim_server_url}/api/runs/{run_id}/jobs/{job_id}'.format(pim_server_url=os.environ.get('PIM_SERVER_URL', 'http://localhost:5000'), run_id=self.run_id, job_id=self.id)

        # Generate json data payload
        payload = jsonpickle.encode(self, unpicklable=False)

        # Post the job(s) to the rest server
        request = requests.put(url, json=json.loads(payload))

        # Inform us when the request was not processed as anticipated
        if request.status_code != 200:
            logger.debug('Unable to post job to PIM: {}, {}'.format(request.status_code, request.text))

    async def start(self):
        """Start job asynchronously

        """

        # Change status to running
        self.status = 'running'

        self.put()

        logger.debug('Starting job "{}"'.format(self.id))

        # Choose how long to wait asynchronously
        wait_ms = random.uniform(self.duration_range[0], self.duration_range[1])

        # This allows us to pretend this job is running on a cluster
        await asyncio.sleep(wait_ms)

        self.status = 'success'

        # Compute the likelihood that a job ends with success
        success_probability = 1.0 - self.failed_probability

        # Fake failed jobs
        if random.random() > success_probability:
            self.status = 'failed'

        self.put()

        logger.debug('Finished job "{}"'.format(self.id))

    def to_json(self):
        """Convert job to JSON

        :return: JSON encoded job
        """

        return json.loads(jsonpickle.encode(self, unpicklable=False))

    def __getstate__(self):
        return dict(id=self.id, run_id=self.run_id, node_id=self.node_id, sample_id=self.sample_id, status=self.status)

    def __repr__(self):
        return str(self.__dict__)
