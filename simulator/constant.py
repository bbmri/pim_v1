from simulator.node import Node


class Constant(Node):
    """Convenience class which models a constant node (consisting of only one output port)

    """

    def __init__(self, **kwargs):
        """Initialize the constant node"""

        # Constant node has only one output port
        kwargs['no_in_ports'] = 0
        kwargs['no_out_ports'] = 1

        super(Constant, self).__init__(**kwargs)

        self.type = 'constant'
