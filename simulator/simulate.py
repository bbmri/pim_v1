import os
import asyncio
import pkgutil

import simulator.examples

from simulator.log import logger


def simulate():
    """Simulates multiple test cases asynchronously

    """

    # Obtain simulation parameters from environment
    os.environ['NO_SAMPLES']        = str(10)
    os.environ['JOB_MIN_DURATION']  = str(1)
    os.environ['JOB_MAX_DURATION']  = str(25)

    # Package where example runs are located
    tests_package = simulator.examples

    # Create event loop
    event_loop = asyncio.get_event_loop()

    # Keep track of which runs are simulated
    runs = list()

    # Iterate over all test cases (submodules) and simulate each of them
    for importer, module_name, ispkg in pkgutil.iter_modules(tests_package.__path__):

        # Import test case module
        case_module = importer.find_module(module_name).load_module(module_name)

        # Create run for test case
        run = case_module.create_run()

        # Add the run to a list so that we can remove it from pim when there is an exception
        runs.append(run)

        logger.debug('Testing {run_id}'.format(run_id=run.id))

        # Send the run to pim using an http put request
        pim_add_request = run.pim_add_run()

        # Http return codes that are acceptable [ok, created, accepted]
        acceptable_status_codes = [200, 201, 202]

        # Except if we get an unacceptable response
        if pim_add_request.status_code not in acceptable_status_codes:
            raise Exception("HTTP PUT request returned an error {status_code}".format(status_code=pim_add_request.text))

    # Asynchronous simulation
    for run in runs:
        run.pim_simulate()

    # Start the event loop
    event_loop.run_forever()


if __name__ == "__main__":
    simulate()
