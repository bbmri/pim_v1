from simulator.log import logger


class Network:
    """Network description class

    """

    def __init__(self, **kwargs):
        """Initialize the network"""

        self.run_id         = kwargs.get('run_id', 'undefined')
        self.description    = ""
        self.nodes          = dict()
        self.links          = list()
        self.groups         = list()

    def add_node(self, type, **kwargs):
        """Add a node to the network

        :param type: Type of network
        :param kwargs: Keyword arguments
        :return: Network
        """

        # Provide the node with the run id
        kwargs['run_id'] = self.run_id

        # Create new node
        node = type(**kwargs)

        # if node.id in self.nodes:
        #     raise ValueError("Unable to add node, node id '{node_id}' is not unique!".format(node_id=node.id))

        self.nodes[node.id] = node

        return node

    def simulate(self):
        """Simulates the network

        This routine simulates an (image processing) pipeline execution engine. For each node n n jobs are executed
        asynchronously. These jobs don't do any actual work, their sole purpose is to fake a time consuming job on a
        HPC cluster. Jobs take a random time to complete. The lower/upper bound is determined by two environment
        variables JOB_MIN_DURATION and JOB_MAX_DURATION respectively.

        """

        logger.debug('Simulating network')

        # Schedule jobs for each node
        for node in self.nodes.values():
            node.schedule_jobs()

        # Simulate root node(s)
        for node_id, node in self.nodes.items():
            if len(node.in_ports) == 0 and len(node.out_ports) == 1:
                node.simulate()

    def __getstate__(self):
        nodes = [node for id, node in self.nodes.items()]

        return dict(description=self.description, nodes=nodes, links=self.links, groups=self.groups)

    def __repr__(self):
        return str(self.__dict__)
