import os
import asyncio

from simulator.port import Port
from simulator.job import Job
from simulator.log import logger
from simulator.signal import Signal


class Node:
    """Node class

    """

    def __init__(self, **kwargs):

        # Default number of input/output ports
        kwargs['no_in_ports']   = kwargs.get('no_in_ports', 4)
        kwargs['no_out_ports']  = kwargs.get('no_out_ports', 4)

        self.id                 = kwargs.get('id', "")
        self.run_id             = kwargs.get('run_id', "undefined")
        self.type               = 'node'
        self.group_id           = kwargs.get('group_id', 'root')
        self.in_ports           = [Port(id="InPort_{0}".format(pid), direction='in') for pid in range(kwargs.get('no_in_ports', -1))]
        self.out_ports          = [Port(id="OutPort_{0}".format(pid), direction='out') for pid in range(kwargs.get('no_out_ports', -1))]
        self.no_samples         = int(os.environ.get('NO_SAMPLES', 25))
        self.jobs               = list()
        self.futures            = list()
        self.jobs_done          = Signal(self)
        self.is_constant        = len(self.in_ports) == 0 and len(self.out_ports) == 1 and self.no_samples == 1

        # Take action when all jobs are done
        self.jobs_done.connect(self.on_jobs_done)

        # We want to start the node simulation when all input ports have finished
        for in_port in self.in_ports:
            in_port.finished.connect(self.in_port_finished)

    def schedule_jobs(self):
        """Generate jobs for the node

        """

        for job_id in range(self.no_samples):
            sample_id = 'sample_{}'.format(job_id)
            failed_probability = 0.05

            if self.is_constant:
                failed_probability = 0

            job = Job(node_id=self.id, run_id=self.run_id, sample_id=sample_id, failed_probability=failed_probability)

            self.jobs.append(job)

            job.put()

    def job_ended(self, job):
        """Callback when a job has finished

        This callback allows us also to check if all jobs have finished, and take additional actions

        :param job: Job that ended
        """

        # If all jobs are done
        jobs_done = True

        # Check the status of all asynchronous jobs
        for f in self.futures:
            if f._state == 'PENDING':

                jobs_done = False

        # Inform observers that all our jobs have completed
        if jobs_done:
            self.jobs_done.fire()

    def on_jobs_done(self, sender):
        """Callback when all jobs have finished

        :param sender: Sender
        """

        logger.debug('All jobs for "{}" finished'.format(self.id))

        for port in self.out_ports:
            port.finished.fire()

    def in_port_finished(self, in_port):
        """Callback when the node of an input port has finished processing

        :param in_port: Input port of which its associated node has finished
        """

        logger.debug('Input port "{}:{}" finished'.format(in_port.id, self.id))

        all_finished = True

        for in_port in self.in_ports:
            if not all_finished:
                break

            for link in in_port.links:
                if link.to_port.state == 'pending':
                    all_finished = False
                    break

        if all_finished:
            logger.debug('All jobs for {} have finished'.format(self.id))
            self.simulate()

    def simulate(self):
        """Simulate the node as though it is being processed in a HPC cluster

        """

        logger.debug('Simulating {} job(s) for "{}"'.format(self.no_samples, self.id))

        # Start jobs asynchronously
        for job in self.jobs:
            self.futures.append(asyncio.ensure_future(job.start()))
            self.futures[-1].add_done_callback(self.job_ended)

    def in_port(self, id):
        """Get input port by ID

        :param id: ID of the input port
        :return: Input port if exists, otherwise None
        """

        for in_port in self.in_ports:
            if in_port.id == id:
                return in_port

        return None

    def out_port(self, id):
        """Get output port by ID

        :param id: Output port ID
        :return: Output port if exists, otherwise None
        """

        for out_port in self.out_ports:
            if out_port.id == id:
                return out_port

        return None

    def __getstate__(self):
        return dict(id=self.id, type=self.type, in_ports=self.in_ports, out_ports=self.out_ports,
                    group_id=self.group_id)

    def __repr__(self):
        return str(self.__dict__)
