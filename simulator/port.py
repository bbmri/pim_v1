from simulator.signal import Signal
from simulator.log import logger


class Port:
    """Attachment point for data flow which can be either input or output

    """

    def __init__(self, **kwargs):
        self.id             = kwargs.get('id', "")
        self.description    = 'undefined'
        self.no_connections = 0
        self.direction      = kwargs.get('direction', 'in')
        self.links          = list()
        self.state          = 'pending'
        self.finished       = Signal(self)

    def finished(self):
        """Triggered when the node (associated with this port) finished processing

        """

        fire = self.state != 'finished'

        self.state = 'finished'

        if fire:
            self.finished.fire()

    def on_link_finished(self, link):
        """Callback when the link has finished

        :param link: Link
        """

        self.state = 'finished'

        self.finished.fire()

    def connect(self, link):
        """Connect to a link

        :rtype: object
        :param link: Link
        """

        if link:
            self.links.append(link)

            if self.direction == 'in':
                link.finished.connect(self.on_link_finished)

            # Increase the number of connections by one
            self.no_connections += 1
        else:
            raise Exception("Unable to connect link to port, link is not defined")

    def is_connected(self):
        """Determines if the port is connected to any link

        :return: If the link is connected
        """

        return self.no_connections > 0

    def __getstate__(self):
        return dict(id=self.id, description=self.description)

    def __repr__(self):
        return str(self.__dict__)
